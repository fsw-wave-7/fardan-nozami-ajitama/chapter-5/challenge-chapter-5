const express = require("express");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const Users = require("../controllers/users.js");
const users = new Users();

const logger = (req, res, next) => {
  console.log(`LOG : ${req.method} ${req.url}`);
  next();
};

const api = express.Router();
api.use(logger);
api.use(jsonParser);

// user
api.post("/user", users.insertUser);
api.get("/users", users.getUsers);
api.get("/user/:index", users.getDetailUser);
api.put("/user/:index", users.updateUser);
api.delete("/user/:index", users.deleteUser);

module.exports = api;
