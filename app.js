const express = require("express");
const app = express();
const api = require("./routes/api.js");
const port = 3000;

// set view engine
app.set("view engine", "ejs");

// load static file
app.use(express.static(__dirname + "/public"));

// route home
app.get("/", (req, res) => {
  res.render("index", {
    content: "./pages/home",
    title: "Home",
  });
});

// route about
app.get("/about", (req, res) => {
  res.render("index", {
    content: "./pages/about",
    title: "About",
  });
});

// route features
app.get("/features", (req, res) => {
  res.render("index", {
    content: "./pages/features",
    title: "Features",
  });
});

// route requirements
app.get("/requirements", (req, res) => {
  res.render("index", {
    content: "./pages/requirements",
    title: "Requirements",
  });
});

// route quotes
app.get("/quotes", (req, res) => {
  res.render("index", {
    content: "./pages/quotes",
    title: "Quotes",
  });
});

// route newsletter
app.get("/newsletter", (req, res) => {
  res.render("index", {
    content: "./pages/newsletter",
    title: "Newsletter",
  });
});

// route game
app.get("/game", (req, res) => {
  res.render("game");
});

// load api routes
app.use("/api", api);

// internal server error handle middleware
app.use(function (err, req, res, next) {
  console.log(err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

// 404 handler middleware
app.use(function (req, res, next) {
  res.status(404).json({
    status: "fail",
    errors: "are you lost?",
  });
});

// menjalankan server di port
app.listen(port, () => {
  console.log("server berhasil dijalankan");
});
