class Game {
  constructor() {
    this.options = [
      {
        name: "batu",
        url: "./img/batu.png",
      },
      {
        name: "kertas",
        url: "./img/kertas.png",
      },
      {
        name: "gunting",
        url: "./img/gunting.png",
      },
    ];
  }

  // membangkitkan bilangan random untuk pilihan Comp
  getPilihanComp() {
    const comp = Math.random();
    if (comp < 0.34) return "batu";
    if (comp >= 0.34 && comp < 0.67) return "kertas";
    return "gunting";
  }

  // rules Game
  getHasil(player, comp) {
    if (player == comp) return "DRAW";
    if (player == "batu") return comp == "kertas" ? "COM WIN" : "PLAYER WIN";
    if (player == "kertas") return comp == "gunting" ? "COM WIN" : "PLAYER WIN";
    if (player == "gunting") return comp == "batu" ? "COM WIN" : "PLAYER WIN";
  }
}

class PlayGame extends Game {
  constructor() {
    super();
  }

  // tampil img dan pilihan player dan klik
  getCardsPlayer() {
    const t = this;
    return this.options.forEach((e) => {
      const img = document.createElement("img");
      img.src = e.url;
      img.className = "imgPlayer mt-md-5 pt-md-3 pb-md-3 pl-md-3 pr-md-3";
      img.onclick = function () {
        const player = e.name;
        console.log(`pilihan player : ${player}`);
        const comp = t.getPilihanComp();
        const imgComp = document.querySelectorAll(".imgComp");
        imgComp.forEach((e) => {
          if (comp == e.id) return e.classList.add("pilihan");
        });
        console.log(`pilihan comp : ${comp}`);
        const hasil = t.getHasil(player, comp);
        console.log(`hasil : ${hasil}`);
        img.classList.add("pilihan");

        let imgHasil = document.querySelector(".result");
        if (hasil == "DRAW") {
          imgHasil.style.backgroundColor = "#035B0C";
        } else {
          imgHasil.style.backgroundColor = "#4C9654";
        }
        imgHasil.classList.add("hasil");
        imgHasil.innerHTML = ` ${hasil} `;
        let gambar = document.querySelectorAll("img");
        let refresh = document.querySelector(".refresh");
        refresh.addEventListener("click", () => {
          gambar.forEach((gbr) => {
            gbr.classList.remove("pilihan");
          });
          let result = document.querySelector(".result");
          result.classList.remove("hasil");
          result.innerHTML = "VS";
          result.style.backgroundColor = "#9c835f";
        });
      };
      document.querySelector(".cardsPlayer").append(img);
    });
  }

  getCardsComp() {
    const t = this;
    return this.options.forEach((e) => {
      const img = document.createElement("img");
      img.src = e.url;
      img.className = "imgComp mt-md-5 pt-md-3 pb-md-3 pl-md-3 pr-md-3";
      img.id = e.name;
      document.querySelector(".cardsComp").append(img);
    });
  }

  // refresh
  refresh() {}
}

const playGame = new PlayGame();
playGame.getCardsPlayer();
playGame.getCardsComp();
playGame.refresh();
